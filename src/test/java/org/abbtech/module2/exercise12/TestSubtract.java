package org.abbtech.module2.exercise12;

import org.abbtech.module2.exercise12.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;

public class TestSubtract {
    Calculator calculator;
    Calculator calculator1;
    Calculator[] calculatorArray;
    Calculator[] calculatorArray1 = new Calculator[0];
    Calculator[] calculators;
    Calculator[] calculatorList;
    Calculator calculator2;

    @BeforeEach
    void init() {
        calculator = new Calculator();
        calculator1 = null;
        calculator2=new Calculator();
        calculatorArray = new Calculator[]{calculator, calculator1};
        calculatorArray1 = new Calculator[]{calculator, calculator2};
        calculators = new Calculator[]{calculator, calculator2};
        calculatorList = new Calculator[]{calculator};
    }

    @Test
    public void subtract() {
        Assertions.assertEquals(33, calculator.subtract(48, 15));
        Assertions.assertEquals(10, calculator.subtract(25, 5));

        Assertions.assertTrue(calculator.subtract(25, 10) > 4);
        Assertions.assertTrue(calculator.subtract(15, 10) > 24, "Value is invalid");

        Assertions.assertFalse(calculator.subtract(20, 7) > 13);
        Assertions.assertFalse(calculator.subtract(10, 7) < 13, "Value is invalid");

        Assertions.assertNotNull(calculator);
        Assertions.assertNotNull(calculator1, "Object is null");

        Assertions.assertNull(calculator, "Object is not null");
        Assertions.assertNull(calculator1);

        Assertions.assertArrayEquals(calculators, calculatorArray); //eyni saydadir lakin ferqli value dadir
        Assertions.assertArrayEquals(calculatorList, calculatorArray); //ferqli saydadir ferqli value
        Assertions.assertArrayEquals(calculators, calculatorArray1);


        Assertions.assertSame(calculator.subtract(16, 7), 9);
        Assertions.assertSame(calculator.subtract(7, 6), 13);
        Assertions.assertSame(calculator.subtractDouble(7, 6), 1); //gozlenilen  double eslinde olan ise int tipidir
        Assertions.assertSame(calculator.subtractDouble(8, 7), (double) 1); //???bunun niye bele oldugunu basa dusmedim axi tipleri eynidir
        Assertions.assertSame(calculator.subtractDouble(16, 7), 9.0); //??? muellimden sorus

        Assertions.assertNotSame(calculator.subtract(13, 4), 8);
        Assertions.assertNotSame(calculator.subtract(13, 4), 9);

        MatcherAssert.assertThat(calculator.subtract(5, 3), equalTo(2));
        MatcherAssert.assertThat(calculator.subtract(5, 3), equalTo(9));
        MatcherAssert.assertThat(calculator.subtractDouble(5, 3), equalTo(2));//tipleri ferqli oldugu ucun error verdi

        MatcherAssert.assertThat(calculator.subtractDouble(5, 3), equalTo((double) 2));
        MatcherAssert.assertThat(calculator.subtractDouble(5, 3), equalTo((double) 3));
    }
}
