package org.abbtech.module2.exercise12;
import org.abbtech.module2.exercise12.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.equalTo;


public class TestDivide {
    Calculator calculator;
    Calculator calculator1;
    Calculator[] calculatorArray;
    Calculator[] calculatorArray1 = new Calculator[0];
    Calculator[] calculators;
    Calculator[] calculatorList;
    Calculator calculator2;

    @BeforeEach
    void init() {
        calculator = new Calculator();
        calculator1 = null;
        calculator2 = new Calculator();
        calculatorArray = new Calculator[]{calculator, calculator1};
        calculatorArray1 = new Calculator[]{calculator, calculator2};
        calculators = new Calculator[]{calculator, calculator2};
        calculatorList = new Calculator[]{calculator};
    }

    @Test
    public void divide() {
        Assertions.assertEquals(8, calculator.divide(120, 15));
        Assertions.assertEquals(15, calculator.divide(25, 5));
        Assertions.assertEquals(8, calculator.divideDouble(120, 15));
        Assertions.assertEquals(30, calculator.divideDouble(25, 5));

        Assertions.assertTrue(calculator.divide(50, 10) > 4);
        Assertions.assertTrue(calculator.divide(3, 5) > 24, "Value is invalid");
        Assertions.assertTrue(calculator.divide(50, 10) > 5);

        Assertions.assertFalse(calculator.divide(70, 7) < 3);
        Assertions.assertFalse(calculator.divide(70, 7) > 3, "Value is invalid");

        Assertions.assertNotNull(calculator);
        Assertions.assertNotNull(calculator1, "Object is null");

        Assertions.assertNull(calculator, "Object is not null");
        Assertions.assertNull(calculator1);

        Assertions.assertArrayEquals(calculators, calculatorArray); //eyni saydadir lakin ferqli value dadir
        Assertions.assertArrayEquals(calculatorList, calculatorArray); //ferqli saydadir ferqli value
        Assertions.assertArrayEquals(calculators, calculatorArray1);


        Assertions.assertSame(calculator.divide(6, 2), 3);
        Assertions.assertSame(calculator.divide(6, 0), 0);
        Assertions.assertSame(calculator.divide(6, 4), 1);//qaliqli bolme
        Assertions.assertSame(calculator.divide(6, 4), 2);
        Assertions.assertSame(calculator.divideDouble(6, 6), 1); //gozlenilen  double eslinde olan ise int tipidir
        Assertions.assertSame(calculator.divideDouble(6, 6), 0);
        Assertions.assertSame(calculator.divideDouble(14, 7), (double) 2); //???bunun niye bele oldugunu basa dusmedim axi tipleri eynidir
        Assertions.assertSame(calculator.divideDouble(35, 7), 5.0); //??? muellimden sorus
        Assertions.assertSame(calculator.divideDouble(8, 6), 2);
        Assertions.assertSame(calculator.divideDouble(8, 6), 1.33);

        Assertions.assertNotSame(calculator.divide(16, 4), 8);
        Assertions.assertNotSame(calculator.divide(16, 4), 4);
        Assertions.assertNotSame(calculator.divideDouble(16, 4), 4);//ferqli tipdir ok
        Assertions.assertNotSame(calculator.multiplyDouble(16, 4), (double) 4);// eyni tipde olsalar da her bir halda eyni olmadignin gosterir niye?

        MatcherAssert.assertThat(calculator.divide(6, 3), equalTo(2));
        MatcherAssert.assertThat(calculator.divide(7, 3), equalTo(2));
        MatcherAssert.assertThat(calculator.divide(7, 3), equalTo(1));
        MatcherAssert.assertThat(calculator.divide(5, 0), equalTo(0));
        MatcherAssert.assertThat(calculator.divideDouble(6, 3), equalTo(2));//tipleri ferqli oldugu ucun error verdi
        MatcherAssert.assertThat(calculator.divideDouble(6, 3), equalTo((double) 2));
       MatcherAssert.assertThat(calculator.divideDouble(6,3),equalTo(2.0));

    }
}
