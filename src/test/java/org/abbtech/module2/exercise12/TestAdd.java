package org.abbtech.module2.exercise12;
import org.abbtech.module2.exercise12.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.equalTo;


public class TestAdd {
    Calculator calculator;
    Calculator calculator1;
    Calculator[] calculatorArray;
    Calculator[] calculatorArray1 = new Calculator[0];
    Calculator[] calculators;
    Calculator[] calculatorList;

    @BeforeEach
    void init() {
        calculator = new Calculator();
        calculator1 = null;
        calculatorArray = new Calculator[]{calculator, calculator1};
        calculatorArray1 = new Calculator[0];
        calculators = new Calculator[]{calculator, calculator1};
        calculatorList = new Calculator[]{calculator};
    }

    @Test
    public void add() {

        Assertions.assertEquals(10, calculator.add(4, 5));
        Assertions.assertEquals(10, calculator.add(5, 5));

        Assertions.assertTrue(calculator.add(5, 10) > 4);
        Assertions.assertTrue(calculator.add(5, 10) > 24, "Value is invalid");

        Assertions.assertFalse(calculator.add(4, 7) > 13);
        Assertions.assertFalse(calculator.add(4, 7) < 13, "Value is invalid");

        Assertions.assertNotNull(calculator);
        Assertions.assertNotNull(calculator1, "Object is null");

        Assertions.assertNull(calculator, "Object is not null");
        Assertions.assertNull(calculator1);

        Assertions.assertArrayEquals(calculators, calculatorArray);
        Assertions.assertArrayEquals(calculatorList, calculatorArray);

        Assertions.assertSame(calculator.add(6, 7), 13);
        Assertions.assertSame(calculator.addDouble(7, 6), 13.0);
        Assertions.assertSame(calculator.addDouble(6, 7), (double) 13);

        Assertions.assertNotSame(calculator.add(3, 4), 8);
        Assertions.assertNotSame(calculator.add(3, 4), 7);

        MatcherAssert.assertThat(calculator.add(5, 3), equalTo(8));
        MatcherAssert.assertThat(calculator.add(5, 3), equalTo(9));
        MatcherAssert.assertThat(calculator.addDouble(5, 3), equalTo(8));//tipleri ferqli oldugu ucun error verdi
        MatcherAssert.assertThat(calculator.addDouble(5, 3), equalTo((double) 8));

    }

}
