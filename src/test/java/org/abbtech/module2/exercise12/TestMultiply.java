package org.abbtech.module2.exercise12;

import org.abbtech.module2.exercise12.Calculator;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.equalTo;

public class TestMultiply {
    Calculator calculator;
    Calculator calculator1;
    Calculator[] calculatorArray;
    Calculator[] calculatorArray1 = new Calculator[0];
    Calculator[] calculators;
    Calculator[] calculatorList;
    Calculator calculator2;

    @BeforeEach
    void init() {
        calculator = new Calculator();
        calculator1 = null;
        calculator2 = new Calculator();
        calculatorArray = new Calculator[]{calculator, calculator1};
        calculatorArray1 = new Calculator[]{calculator, calculator2};
        calculators = new Calculator[]{calculator, calculator2};
        calculatorList = new Calculator[]{calculator};
    }

    @Test
    public void multiply() {
        Assertions.assertEquals(120, calculator.multiply(8, 15));
        Assertions.assertEquals(30, calculator.multiply(25, 5));

        Assertions.assertTrue(calculator.multiply(25, 10) > 4);
        Assertions.assertTrue(calculator.multiply(3, 5) > 24, "Value is invalid");

        Assertions.assertFalse(calculator.multiply(10, 7) < 13);
        Assertions.assertFalse(calculator.multiply(10, 7) > 13, "Value is invalid");

        Assertions.assertNotNull(calculator);
        Assertions.assertNotNull(calculator1, "Object is null");

        Assertions.assertNull(calculator, "Object is not null");
        Assertions.assertNull(calculator1);

        Assertions.assertArrayEquals(calculators, calculatorArray); //eyni saydadir lakin ferqli value dadir
        Assertions.assertArrayEquals(calculatorList, calculatorArray); //ferqli saydadir ferqli value
        Assertions.assertArrayEquals(calculators, calculatorArray1);


        Assertions.assertSame(calculator.multiply(6, 7), -42);
        Assertions.assertSame(calculator.multiply(6, 7), 42);
        Assertions.assertSame(calculator.multiply(6, 7), 10);
        Assertions.assertSame(calculator.multiplyDouble(7, 6), 42); //gozlenilen  double eslinde olan ise int tipidir
        Assertions.assertSame(calculator.multiplyDouble(7, 6), 10);
        Assertions.assertSame(calculator.multiplyDouble(8, 7), (double) 56); //???bunun niye bele oldugunu basa dusmedim axi tipleri eynidir
        Assertions.assertSame(calculator.multiplyDouble(5, 7), 35.0); //??? muellimden sorus

        Assertions.assertNotSame(calculator.multiply(13, 4), 8);
        Assertions.assertNotSame(calculator.multiply(13, 4), 52);
        Assertions.assertNotSame(calculator.multiplyDouble(13, 4), 52);
        Assertions.assertNotSame(calculator.multiplyDouble(13.0, 4), (double) 52);// eyni tipde olsalar da her bir halda eyni olmadignin gosterir niye?

        MatcherAssert.assertThat(calculator.multiply(6, 3), equalTo(18));
        MatcherAssert.assertThat(calculator.multiply(7, 3), equalTo(2));
        MatcherAssert.assertThat(calculator.multiplyDouble(7, 0), equalTo(0));
        MatcherAssert.assertThat(calculator.multiplyDouble(7, 0), equalTo((double) 0));
        MatcherAssert.assertThat(calculator.multiplyDouble(6, 3), equalTo(18));//tipleri ferqli oldugu ucun error verdi
        MatcherAssert.assertThat(calculator.multiplyDouble(6, 3), equalTo((double) 18));
        MatcherAssert.assertThat(calculator.multiplyDouble(6, 3), equalTo(18.0));
    }
}