package org.abbtech.module2.exercise13;

import org.abbtech.module2.exercise13.User;
import org.abbtech.module2.exercise13.UserManager;
import org.abbtech.module2.exercise13.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserTest {
    @Mock
    private UserRepository userRepository;


    @InjectMocks //repositoryni managere inject edir
    private UserManager userManager;


    User user = new User("zemine ", 1L, true);
    User user2 = new User("ali ", 2L, false);
    User user3 = new User("ayan ", 3L, true);

    @Test
    public void isUserActive() {
        Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        //Mockito.when(userRepository.findByUsername(userName()).isActive()==true).thenReturn(user2.isActive());
        Assertions.assertTrue(userManager.isUserActive(user.getUsername()));
        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(user.getUsername());
    }

    void getUser() throws Exception {
        Long userId = 1L;
        Long userId2 = 2L;

        Mockito.when(userRepository.findUserId(user2.getUserId())).thenReturn(user2);
//sertin dendiyi hal
        Assertions.assertEquals(user2, userManager.getUser((userId2)));
        Mockito.verify(userRepository, Mockito.times(1)).findUserId(userId2);
        ///sertin odenmediyi hal
        Assertions.assertEquals(user2,userManager.getUser((userId)));
        Mockito.verify(userRepository,Mockito.times(1)).findUserId(userId);


    }
    public void deleteUser() throws Exception {
        Long userId3 = 3L;
        Mockito.when(userRepository.findUserId(user3.getUserId())).thenReturn(user3);
        Assertions.assertEquals(user3,userManager.getUser(userId3));
        Assertions.assertDoesNotThrow(() -> userManager.deleteUser(user3.getUserId()));
        Mockito.verify(userRepository, Mockito.times(1)).findUserId(userId3);
    }
}
