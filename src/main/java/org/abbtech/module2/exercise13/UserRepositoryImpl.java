package org.abbtech.module2.exercise13;

import java.util.Objects;

public class UserRepositoryImpl implements UserRepository{
    User user;
    @Override
    public User findByUsername(String username) {
       if ( user.getUsername().equals(username))return user;
      else   return null;
    }

    @Override
    public User findUserId(Long userId) {
        if (Objects.equals(user.getUserId(), userId))return user;
        else return null;
    }
}
