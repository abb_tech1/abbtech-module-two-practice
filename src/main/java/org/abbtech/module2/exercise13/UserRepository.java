package org.abbtech.module2.exercise13;

public interface UserRepository {
    User findByUsername(String username);

    User findUserId(Long userId);

}
