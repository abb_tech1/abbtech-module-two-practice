package org.abbtech.module2.exercise13;

public class UserManager {
    UserRepository userRepository;

    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(Long userId) throws Exception {
        User user = userRepository.findUserId(userId);
        if (user == null) {
            throw new Exception();
        } else user = null;
    }

    public User getUser(Long userId) throws Exception {
        User user = userRepository.findUserId(userId);
        if (user == null) {
            throw new Exception();
        }
        return user;
    }

}
