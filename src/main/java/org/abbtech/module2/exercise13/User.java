package org.abbtech.module2.exercise13;

public class User {
    private String username;
    private Long userId;
    private boolean isActive;

    public boolean isActive() {
        return isActive;
    }

    public User() {
    }

    public User(String username, Long userId, boolean isActive) {
        this.username = username;
        this.userId = userId;
        this.isActive = isActive;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
