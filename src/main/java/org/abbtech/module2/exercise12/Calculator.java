package org.abbtech.module2.exercise12;

public class Calculator {
    //    The Calculator class should have methods for addition, subtraction, multiplication, and division.
//Each method should take two parameters and return the result of the operation.
//Write separate test methods for each operation.
//Use different types of assertions such as assertEquals, assertTrue, assertFalse, assertNull, assertNotNull, assertArrayEquals, assertSame, assertNotSame, assertThat, etc.
//Test cases should cover both positive and negative scenarios, including edge cases and boundary conditions.
//Ensure that your tests are readable and maintainable.
    public double addDouble(double a, double b) {
        return (a + b);

    }

    public int add(int a, int b) {
        return (a + b);

    }

    public double subtractDouble(double a, double b) {
        return a - b;

    }

    public int subtract(int a, int b) {
        return (a - b);

    }

    public double multiplyDouble(double a, double b) {
        return a * b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public double divideDouble(double a, double b) {
        return a / b;
    }

    public int divide(int a, int b) {
        return a / b;
    }
}
